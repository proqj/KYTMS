package com.kytms.core.constants;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 臧英明
 * 符号
 *
 * @author
 * @create 2018-01-15
 */
public class Symbol {
    public static final String COMMA = ",";
    public static final String HYPHEN = "-";
    public static final String DOT = "'";
}
