package com.kytms.core.utils;

import com.kytms.core.constants.Symbol;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 *
 * SQL工具类
 *
 * @author 臧英明
 * @create 2018-01-18
 */
public abstract class SqlUtils {
    /**
     * 将数组字符串切割成in函数 如：'234','3123','123123'
     * @param split
     * @return
     */
    public static String splitForIn(String[] split){
        StringBuilder sb = new StringBuilder();
        for (String str:split) {
            sb.append(Symbol.DOT).append(str).append(Symbol.DOT).append(Symbol.COMMA);
        }
        sb.deleteCharAt(sb.lastIndexOf(Symbol.COMMA));
        return sb.toString();
    }
}
