package com.kytms.rbac.dao.Impl;

import com.kytms.core.dao.impl.BaseDaoImpl;
import com.kytms.core.entity.User;
import com.kytms.rbac.dao.UserDao;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * @author 臧英明
 * @create 2017-11-18
 */
@Repository(value = "UserDao")
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao<User> {
    private final Logger log = Logger.getLogger(UserDaoImpl.class);//输出Log日志
}
